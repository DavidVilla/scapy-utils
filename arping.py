#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import scapy.all as scapy

# arping.py
#
# Copyright (C) 2007,2008 David Villa Alises
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


import sys, socket, os
import scapy.all as scapy  # scapy 2.0.0.5
OUI = '/tmp/oui.txt'

def get_oui(mac):

    if not os.path.exists(OUI):
        print "Feching OUIs...",
        sys.stdout.flush()
        os.system('wget http://standards.ieee.org/regauth/oui/oui.txt -O %s 2> /dev/null' % OUI)
        print "done."

    oui = mac[:8].replace(':','')
    for line in file(OUI):
        if not '(base 16)' in line: continue
        if line.lower().startswith(oui):
            return "%s" % line[22:].strip()

    return '<not found>'

if len(sys.argv) not in [2,3]:
    print "Usage: arping <MAC> [net]"
    print "ex: arping 00:11:22"
    print "ex: arping 00:11:22:33:44:55 192.168.1.0/24"
    sys.exit(1)


if os.getuid() != 0:
    print "Error: You need admin priviledges to run that program."
    sys.exit(1)

networks = []
if len(sys.argv) == 3:
    networks.append(sys.argv[2])
else:
    # Trying to find "public" ifaces
    tobin = lambda n: n>0 and tobin(n>>1)+str(n&1) or ''
    for net,msk,gw,iface,addr in scapy.conf.route.routes:
        if iface != 'lo' and gw == '0.0.0.0':
            ip = scapy.ltoa(net)
            mask = tobin(msk).count('1')
            networks.append("%s/%s" % (ip, mask))


for net in networks:
    print 'Using network:', net

    scapy.conf.verb=0
    ans,unans = scapy.srp(scapy.Ether(dst="ff:ff:ff:ff:ff:ff")/
                          scapy.ARP(pdst=net),
                          timeout=2)

    #FIXME: sort IP addresses
    for request,reply in ans:
        if reply.hwsrc.lower().startswith(sys.argv[1].lower()):
            print "%15s - %s - %s" % (reply.psrc, reply.hwsrc,
                                      get_oui(reply.hwsrc))
